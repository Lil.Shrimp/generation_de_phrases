import re
from scipy.spatial import distance

embeddings_path = "./Ressources/embeddings-Fr.txt"
table_associative_path = "./Ressources/TableAssociative"
template = "./Ressources/templates_basiques"
template2 = "./Ressources/templates_basiques_2"
template3 = "./Ressources/templateEval"
fichierEvaluation = "fichierEvaluation.txt"

embeddingDictionnary = {} # dictionnaire pour embedding
tableAssociativeDictionnary = {}  # dictionnaire pour table associative
listePhrases = [] # liste pour les phrases template
listePhrases2 = []
listePhrases3 = []

def embeddingDict(dictionnaire): # fonction qui crée le dictionnaire à partir du fichier embedding
    with open(embeddings_path, 'r', encoding="utf8") as f:
       for line in f.readlines():
            word = re.sub(r""",|\[|]|\n""", "", line)
            key, value = word.split('\t')
            dictionnaire[key] = value
    return dictionnaire

def tableAssociativeDict(dictionnaire): # fonction qui crée le dictionnaire à partir du fichier tableAssociative
    with open(table_associative_path, 'r', encoding="utf8") as f:
        for line in f.readlines():
            word = re.sub(r""",|\n""", "", line)
            key, value = word.split('\t', maxsplit=1)
            value = value.split('\t')
            dictionnaire[key] = value
    return dictionnaire

def getPhrases(listePhrases): # fonction qui retourne dans une liste les phrases des fichier template
    with open(template, 'r', encoding="utf8") as f:
        for line in f.readlines():
            line = re.sub(r""",|\n""", "", line)
            line = line.replace("_", " ")
            listePhrases.append(line)
    with open(template2, 'r', encoding="utf8") as file:
        for line in file.readlines():
            line = re.sub(r""",|\n""", "", line)
            line = line.replace("_", " ")
            listePhrases2.append(line)
    with open(template3, 'r', encoding="utf8") as file:
        for line in file.readlines():
            line = re.sub(r""",|\n""", "", line)
            line = line.replace("_", " ")
            listePhrases3.append(line)
    return listePhrases, listePhrases2, listePhrases3

def setUp(dictEmb, dictTA, liste): # prépare les dictionnaires et les liste de phrase provenant des templates en faisant appelle aux méthodes respectives
    embeddingDict(dictEmb)
    tableAssociativeDict(dictTA)
    getPhrases(liste)
    return dictEmb, dictTA, liste

def getMotsAssocies(typeMot):  # récupère la liste des mots associés à celui du type de mot voulu (sert pour l'étiquette)
    for i in tableAssociativeDictionnary:  # on parcour le dictionnaire
        if (i == typeMot):
            liste = tableAssociativeDictionnary[i]
            return liste

def getEmbedding(mot):  # récupère le vecteur embedding du mot en paramètre
    for i in embeddingDictionnary:  # on parcour le dictionnaire
        if (i == mot): # quand on trouve le mot correspondant
            vecteur = embeddingDictionnary[i] # on récupère son embeddig dans une variable
            return vecteur # que l'on retourne

def distanceEuclidienne(mot1, mot2): # calcul la distance euclidienne entre deux vecteurs
    a = makeVectors(mot1) # on transforme les vecteurs pour l'instant sous forme de string en véritables vecteurs
    b = makeVectors(mot2) # on transforme les vecteurs pour l'instant sous forme de string en véritables vecteurs
    dist = distance.euclidean(a, b) # calcul de la distance euclidienne
    return dist # on retourne cette distance

def makeVectors(vecteurString): # convertie la value string d'une key dans le dictionnaire en un vecteur de float
    floats = [float(x) for x in vecteurString.split()]
    return floats

def traitement(phrase): # fonction qui va pour une phrase retourner les étiquettes et thèmes
    listMotCles = [] # liste qui contiendra les mots etiquettes et thèmes de chaque mot à remplacer de la phrase
    i, j= 0, 1 # compteurs
    while i < len(phrase) : # on parcours la string caractère par caractère
        if phrase[i] == "*" : # lorsque l'on trouve le caractère annonçant un mot à placer
            words = phrase.split('*', maxsplit=j)[-1] \
                .split(maxsplit=1)[0].split("/") # on isole l'étquette et les deux thèmes
            etiquette = words[0]  # l'etiquette
            mot1 = words[1] # thème1
            mot2 = words[2] # thème 2
            motCles = (etiquette, mot1, mot2) # on les rassemble
            listMotCles.append(motCles) # on ajoute chaque ensemble dans la liste finale
            i += 1 # on incrémente
            j += 1 # on incrémente
        else : i += 1 # on incrémente
    print (listMotCles) # pour visuellement avoir l'information sur les mots clés bien repérés
    return listMotCles # on retourne la liste des enemble de mots clés

def meilleurMots(phrase, theme): # trouve les mots appropriés pour la phrase # plus tard boucle for sur tt les ensembl de mots clés trouvés
    meilleurMot = '' # contient ce qui sera le meilleur mot jusqu'à présent trouvé pour combler le trou
    motsIdeal = [] # liste des mots idéals pour la phrases dans l'ordre dans lequel ils seront placés dans les différents trous
    motCles = traitement(phrase) # appel de la méthode traitement() pour récupérer les ensembles de mots clés
    embeddingTheme = getEmbedding(theme)  # appel de la méthode getEmbedding() pour récupérer l'ensemble des embeddings des mots clés
    i = 0 # compteur
    while i < len(motCles) : # on parcours la liste des ensembles de mots clés
        bestDistance = 10 # variable qui contiendra la meilleure distance trouvée pour le moment, correspondant au mot idéal actuel
        motsAssocies = getMotsAssocies(motCles[i][0]) # récupère la liste des mots associés à l'étiquette
        embeddingThemeInterdit = getEmbedding(motCles[i][1]) # récupère l'embedding du thème 1
        embeddingThemeInterdit2 = getEmbedding(motCles[i][2]) # récupère l'embedding du thème 2
        for j in range(len(motsAssocies)): # on parcours l'ensemble des mots associés à l'étiquette de l'ensemble de mots clés en cours
            iEmbedding = getEmbedding(motsAssocies[j]) # on récupère l'embedding du mot de la liste actuel
            if iEmbedding is not None and embeddingThemeInterdit2 is not None and motsAssocies[j] != theme and motsAssocies[j] != motCles[i][1] and motsAssocies[j] != motCles[i][2] and motsAssocies[j] not in motsIdeal :
                dist1 = distanceEuclidienne(embeddingTheme, iEmbedding) # distance euclidienne entre embeddings de query et mot actuel
                dist2 = distanceEuclidienne(embeddingThemeInterdit2, iEmbedding) # distance euclidienne entre embeddings de thème interdit et mot actuel
                dist = dist1 + dist2 # somme des distance
                if dist < bestDistance: # si inférieur à la distance globale la plus faible actuelle
                    bestDistance = dist # on prend cette valeur comme bestDistance
                    meilleurMot = motsAssocies[j] # on prend ce mot comme meilleur mot actuel
        motsIdeal.append(meilleurMot) # on ajoute le meilleur mot trouvé pour l'étiquette actuelle dans la liste des mots à rajouter à la phrase
        i += 1 # on incrémente
    print (motsIdeal)
    return motsIdeal # à la fin, on retourne la liste des mots à placer dans le texte

def affichage(phrase, listMotsIdeals) : # fonction qui pour une phrase va remplacer chaque étiquette par le mot idéal correspondant
    i, j = 0, 0 # compteurs
    while i < len(phrase): # on parcours la phrase
        if phrase[i] == "*": # quand on trouve le caractère suivi de la partie à remplacer
            words = phrase.split('*', maxsplit=j+1)[-1] \
                .split(maxsplit=1)[0] # on l'isole dans une variable
            phrase = phrase.replace(words, listMotsIdeals[j]) # on remplace la partie à remplacer par le mot idéal correspondant
            i += 1 # on incrémente
            j += 1 # on incrémente
        else:
            i += 1 # on incrémente
    phrase = phrase.replace('*', '') # on retire les indicateurs
    print(phrase)
    return phrase # on retourne la phrase complétée

def traitementFichierTemplate(phrase, theme) : # fonction qui écrit les phrases complétées dans un fichier txt en utf-8
    file_object = open(fichierEvaluation, 'a', encoding="utf-8") # on ouvre le fichier dans lequel seront écrites les phrases résultant de l'ensemble des traitements
    mots = meilleurMots(phrase, theme) # on appelle la fonction meilleurMots pour la phrase et le thème passé en paramètre
    phrase = affichage(phrase, mots) # on appelle la fonction affichage qui écrit la phrase finale
    file_object.write(phrase + '\t' + theme + '\n') # on écrit la phrase suivi du thème comme demandé dans l'énoncé du rapport final dans le fichier prévu à cet effet

def generationEtEcriture(fichierTemplate, theme) : # appelle l'ensemble des processus de complétion de phrase sur les phrases du fichier selon le thème passé en paramètre
    for i in range(len(fichierTemplate)) :
        traitementFichierTemplate(listePhrases3[i], theme)
    return

if __name__ == "__main__":
    setUp(embeddingDictionnary, tableAssociativeDictionnary, listePhrases) # on appelle la méthode qui rempli les dictionnaires et la liste des phrases template
    generationEtEcriture(template3, "amour")