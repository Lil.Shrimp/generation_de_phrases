import nltk, re, os
from nltk.util import ngrams

####################Lien Vers le Git####################
#https://gitlab.com/Lil.Shrimp/generation_de_phrases.git
########################################################

def extract_ngrams(num, dict): #rempli un dictionnaire avec des n-grams de la taille passée par le paramètre num
    file = open("MEGALITE_FR/A/Abbot,_Edwin-Flatland.pdf.seg","r") #on a utilisé jusqu'à présent un seul document pour plus de fluidité dans les tests
    lines = file.readlines() #on lit le fichier ligne par ligne
    list_grams = []
    for line in lines: #pour chaque ligne lue
        line = re.sub(r"[-()\"#/@;:<>{}=~\’«»|!.?,]", " ", line) #on applique la regex
        line = re.sub(r"[0-9]", "", line) #on applique la regex
        line = line.lower() #on applique la regex
        n_grams = ngrams(nltk.word_tokenize(line), num) #génération des grams
        gram = [' '.join(grams) for grams in n_grams]
        for i in gram:
            list_grams.append(i) #on ajoute chaque gram dans la liste crée préalablement
    return dictionnary(list_grams, dict) #on appelle la fonction qui va remplir le dictionnaire avec les grams

def dictionnary(n_grams, dict): #rempli un dictionnaire à partir d'une liste de grams
    for i in n_grams:
        if (str(i) in dict): #si gram déjà présent dans le dictionnaire
            dict[str(i)] += 1 #on +1 son occurence
        else:
            dict[str(i)] = 1 #sinon on l'ajoute simplement en mettant sa value à 1
    return dict

def generateBigrams(dict, phrase, longueur, mot): #modèle de génération de phrases avec un dictionnaire de bi-grams
    tmpWord = mot
    if (phrase=="") : #si phrase vide on chosi le premier mot
        tmpWord = input("Entrez le premier mot :\n")
        phrase += tmpWord + " " #ajout du mot dans la phrase finale
    while len(phrase.split(" ")) < longueur : #tant qu'on a pas atteint le nbr de mot voulu dans la phrase à générer
        for i in dict : #on parcour le dictionnaire
            occur = 0
            premierMot = i.split(" ") #on effectue un split du bigram
            if (premierMot[0] == tmpWord) : # si le premier mot du bigram est celui du mot ajouté en dernier dans la phrase
                if (dict[i] > occur) : # et si l'occurence du bigram est celle la plus élevée parmis les bigrams candidats
                    tmp = i #on stock le bi gram en question
                    occur = dict[i] # et son occurence
        tmpSplit = tmp.split(" ") #on split le bi gram choisi au final
        tmpWord = tmpSplit[1] #on récupère le second mot du bigram
        phrase += tmpWord + " " # et on l'ajoute à la phrase
    phrase += "."
    return phrase

def generateTrigrams(dict, phrase, longueur, mot): #modèle de génération de phrases avec un dictionnaire de tri-grams
    tmpWord = mot
    if (phrase=="") : #si phrase vide on chosi le premier mot
        tmpWord = input("Entrez le premier mot :\n")
        phrase += tmpWord + " " #ajout du mot dans la phrase finale
    while len(phrase.split(" ")) < longueur : #tant qu'on a pas atteint le nbr de mot voulu dans la phrase à générer
        for i in dict : #on parcour le dictionnaire
            occur = 0
            premierMot = i.split(" ") #on effectue un split du trigram
            if (premierMot[0] == tmpWord) : # si le premier mot du bigram est celui du mot ajouté en dernier dans la phrase
                if (dict[i] > occur) : # et si l'occurence du trigram est celle la plus élevée parmis les bigrams candidats
                    tmp = i #on stock le tri gram en question
                    occur = dict[i] #et son occurence
        tmpSplit = tmp.split(" ") #on split le bi gram choisi au final
        tmpWord = tmpSplit[1] #on récupère le second mot du bigram
        tmpWord2 = tmpSplit[2] # et on l'ajoute à la phrase
        phrase += tmpWord + " " + tmpWord2 + " "
    phrase += "."
    return phrase

if __name__ == '__main__':
    dict = {}
    dict2 = {}
    extract_ngrams(2, dict)
    extract_ngrams(3, dict2)
    #print(dict)
    #print(dict2)
    #phraseGenere = generateBigrams(dict, "", 15, "") #pour tester la fonction bi grams
    phraseGenere = generateTrigrams(dict2, "", 15, "") #pour tester la fonction tri grams
    print(phraseGenere)






